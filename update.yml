# ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
# Summary: Define a reusable CI component for running 'poetry update'.
# Usage:
#   To use this component in your CI/CD configuration, add the following snippet to your
#     .gitlab-ci.yml file:
#
#      include:
#        - project: poetry-tools/ci-cd
#          file: update.yml
#
#   The updated 'poetry.lock' file will be published as a job artifact
#
#   See also:
#     - https://docs.gitlab.com/ee/ci/yaml/includes.html#set-input-parameter-values-with-includeinputs
#     - https://python-poetry.org/docs/cli/#update
#     - https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html
#
# Created: 2023-05-08 00:48:25
# Author:  Bryant Finney <bryant.finney@alumni.uah.edu> (https://bryant-finney.github.io/about)
# ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
spec:
  inputs:
    artifacts-expire-in:
      default: 1 month
    cli-args: # pass additional args to 'poetry build'; use whitespace delimiters
      default: --verbose
    dotenv-file: # a dotenv file consisting of KEY=value pairs to export to the shell
      default: ""
    image: # the docker image to use for the job
      default: python:alpine

---
# export the variables defined in the dotenv-file input
.maybe-export-env-vars: &maybe-export-env-vars |-
  if [ -f "$[[ inputs.dotenv-file ]]" ]; then
    set -o allexport
    . "$[[ inputs.dotenv-file ]]"
    set +o allexport
  fi

# install poetry system dependencies + git
.maybe-install-system-deps: &maybe-install-system-deps |-
  if command -v apk >/dev/null; then
    apk add --update gcc musl-dev libffi-dev curl git
  elif command -v apt-get >/dev/null; then
    apt-get update && apt-get install --no-install-recommends -y curl git
  fi

# install poetry if it is not already installed
.maybe-install-poetry: &maybe-install-poetry |-
  if ! command -v poetry >/dev/null 2>&1; then
    export POETRY_HOME="${POETRY_HOME:-/usr/local}"
    curl -sSL https://install.python-poetry.org | python3 -
  fi

# print the poetry version for debugging purposes
.maybe-print-poetry-version: &maybe-print-poetry-version |-
  if [ -n "$CI_DEBUG_TRACE" ]; then
    which python3; python3 --version
    which poetry ; poetry  --version
  fi

.maybe-install-poetry-dynamic-versioning: &maybe-install-poetry-dynamic-versioning |-
  if grep -q tool.poetry-dynamic-versioning pyproject.toml >/dev/null; then
    poetry self add 'poetry-dynamic-versioning[plugin]'
  fi

⚙️ poetry update:
  artifacts:
    expire_in: $[[ inputs.artifacts-expire-in ]]
    paths: [poetry.lock]
  image: $[[ inputs.image ]]
  script:
    - *maybe-export-env-vars
    - *maybe-install-system-deps
    - *maybe-install-poetry
    - *maybe-print-poetry-version
    - printf '%s\n' "$[[ inputs.cli-args ]]" | xargs poetry update
  stage: build
